#Autor: Miroslav Harag (xharag02)
#Datum: 4.4.2021
#Popis: controller
#Verzia: 1 

def compute(expression,base,precision):
    '''
    Funkcia prijma na vstupe matemticky vyraz. 
    Vracia hodnotu zadaneho vyrazu v sustave so zakladom base s presnostou na precision desatinnych miest.
    Funkcia rozpoznava operacie  "+", "-","*" "/", "%", "^", "sin", "cos", "tan", "factorial" a zatvorky "(",")"
    Vysledok neobsahuje predpony ani pripony ako napr. ("0x...","...b")
    '''
    # 1. zisti pocet operacii vo vyraze. Ak je pocet operacii mensi alebo rovny 1 pokracuje na bod 3.
    # 2. rozdeli vyraz podla operacie s najvyssou prioritou a rekurzivne zavola samu seba na operandy tejto operacie.
    # 3. zisti hodotu vyrazu (pomocou funkcie eval)
    # 4. prevedie vysledok na pozadovany tvar (pomocou funkcie numToStr)
    return

