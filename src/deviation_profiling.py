##@file deviation_profiling.py
# @brief Program pocita smerodajnu odchylku pre ucely profilingu 
# @author Miroslav Harag, xharag02

from mathlib import *
import sys

sum_of_x = 0
sum_of_sqrt = 0
n = 0

for line in sys.stdin:
    for x in line.split():
        # sum_of_x += x
        sum_of_x = eval(str(sum_of_x)+"+"+str(x))
        # sum_of_sqrt += x^2
        sum_of_sqrt = eval(str(sum_of_x)+"+"+str(x)+"^2")
        # n += 1
        n = eval(str(n)+"+1")

s = eval("((1/("+str(n)+"-1))*("+str(sum_of_sqrt)+"-"+str(x)+"^2))^(1/2)")

print("Vyberova smerodatna odchylka:"+str(s))
