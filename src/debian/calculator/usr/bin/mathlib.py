##@file mathlib.py
# @brief Knižnica s matematickými operáciami, a konverzia argumentov
# @author Lucia Jančeková xjance01, Matúš Remeň xremen01

from convertor import *

##Funkcia na vypočítanie súčtu
#@param x Prvý sčítanec
#@param y Druhý sčítanec
#@return Súčet dvoch parametrov.
def add(x, y):

    return x+y

##Funkcia na vypočítanie rozdielu
#@param x Menšenec
#@param y Menšiteľ
#@return Rozdiel dvoch parametrov.
def sub(x, y):

    return x-y

##Funkcia na vypočítanie súčinu
#@param x Prvý činiteľ
#@param y Druhý činiteľ
#@return Súčin dvoch parametrov.
def mul(x, y):

    return x*y

##Funkcia na vypočítanie podielu
#@param x Delenec
#@param y Deliteľ - nemôže byť nula
#@return  Podiel dvoch parametrov.
def div(x, y):

    if y == 0:
        raise ValueError("MATH ERROR: Delenie nulou.\n")
    return x / y

##Funkcia na vypočítanie zvyšku po delení
#@param x Delenec
#@param y Deliteľ - nemôže byť nula
#@return  Zvyšok po delení.
def mod(x, y):

    if y == 0:
        raise ValueError("MATH ERROR: Delenie nulou.\n")
    if isinstance(y, float) and x != 0:
        raise ValueError("Fukncia mod nepodporuje delitel typu float.\n")
    return x % y

##Funkcia na vypočítanie mocniny
#@param x Mocnenec - základ mocniny
#@param y Mocniteľ - exponent
#@return y-tá mocnina čísla x.
def pow(x, y):

    if x == 0 and y < 0:
        raise ValueError("MATH ERROR: Záporný exponent na čislo nula.")
    result = x ** y
    if isinstance(result, complex):
        raise ValueError("Power nepodporuje komplexné čísla.")
    return result

##Funkcia na vypočítanie odmocniny
#@param x Odmocnenec - základ odmocniny
#@param y Uroveň odmocniny
#@return y-tá odmocnina čísla x.
def root(x, y):

    if y <= 0 or (x == 0 and y != 1):
        raise ValueError("MATH ERROR: Takáto odmocnina neexistuje.")
    result = x ** (1 / y)
    if isinstance(result, complex):
        raise ValueError("Power nepodporuje komplexné čísla.")
    return result


# pokrocile matematicke operacie

##Funkcia na výpočet goniometrickej funkcie sínus (Sínus kričí na kone, protilahlá k prepone)
#@param x Velkost uhla v stupňoch
#@return Sínus uhla x
def sin(x):

    pi = findConst("pi")
    sin = {
        0: 0,
        30: 0.5,
        45: root(2, 2) / 2,
        60: root(3, 2) / 2,
        90: 1,
        120: root(3, 2) / 2,
        135: root(2, 2) / 2,
        150: 0.5
    }
    x = mod(x, 360)
    sign = 1 if x < 180 else -1
    x = mod(x, 180)

    if x in sin.keys():
        return sign * sin[x]

    x = x * pi / 180
    precision = 9
    sinus = 0
    for i in range(precision):
        sinus += pow(-1, i) * pow(x, 2 * i + 1) / factorial(2 * i + 1)

    return sign * sinus

##Funkcia na výpočet goniometrickej funkcie kosínus
#@param x Velkost uhla v stupňoch
#@return Kosínus uhla x
def cos(x):

    pi = findConst("pi")
    cos = {
        0: 1,
        30: root(3, 2) / 2,
        45: root(2, 2) / 2,
        60: 0.5,
        90: 0,
        120: -0.5,
        135: -root(2, 2) / 2,
        150: -root(3, 2) / 2
    }
    x = mod(x, 360)
    sign = 1 if x < 180 else -1
    x = mod(x, 180)

    if x in cos.keys():
        return sign * cos[x]

    x = x * pi / 180
    precision = 9
    cosinus = 0
    for i in range(precision):
        cosinus += pow(-1, i) * pow(x, 2 * i) / factorial(2 * i)

    return sign * cosinus

##Funkcie na výpočet goniometrickej funkcie tangens
#@param x Velkost uhla v stupňoch
#@return Tangens uhla x
def tan(x):

    cosinus = cos(x)
    if cosinus == 0:
        raise ValueError("Tangens nie je definovaný v tejto hodnote.")
    return sin(x) / cosinus

##Funkcia na výpočet faktoriálu z cisla x
#@param x Základ faktoriálu
#@return Faktoriál zo základu x
def factorial(x):

    if x == 0.0:
        x = 0
    if x < 0 or isinstance(x, float):
        raise ValueError("Faktoriál sa dá vypočítať iba z kladného celého čísla.")

    if x == 0:
        return 1

    for i in range(x - 1, 0, -1):
        x *= i

    return x

##Funkcia vracia indexy začiatku lavého operandu a konca pravého operandu
#@param exp Výraz, s ktorým sa pracuje
#@param ops_i Index operátora ku ktorému sa hladajú operandy
#@return Dvojica začiatočného indexu lavého operandu a koncového indexu pravého operandu
def getOperandIndexes(exp: str, ops_i: int) -> tuple:

    beg_i = end_i = ops_i
    exp_len = len(exp)
    while beg_i - 1 >= 0 and (exp[beg_i - 1].isdigit() or exp[beg_i - 1] == "."):
        beg_i -= 1

    if end_i + 1 < exp_len and exp[end_i + 1] == "-":
        end_i += 1

    while end_i + 1 < exp_len and (exp[end_i + 1].isdigit() or exp[end_i + 1] == "."):
        end_i += 1

    if beg_i > 0 and exp[beg_i - 1] == "-":
        beg_i -= 1

    return (beg_i, end_i)

##Funkcia hladá pár k zátvorke
#@param subexp Podvýraz, ktorý začína otváracou zátvorkou
#@return Index párovej zátvorky alebo 0 ak pár nebol nájdený == chyba výrazu
def findParenthesesMatch(subexp: str) -> int:

    match = 1
    # prvy znak je otvaracia zatvorka, match je nastaveny na 1, staci hladat podvyraz od 2. znaku
    for i in range(1, len(subexp)):
        if subexp[i] == "(":
            match += 1
        elif subexp[i] == ")":
            match -= 1
            if match == 0:
                return i

    return 0


##Funkcia na odstranenie zduplikovanych operátorov po konverzii kombinácií +-
# Odstraňuje napríklad: √--5 -(.replace)-> √+5 -(__func__)-> √5
# @param exp Matematický výraz
# @param ops Zoznam matematických operátorov
# @return Matematický výraz bez zduplíkovaných operátorov
def removeDoubleOperator(exp: str, ops: list) -> str:

    if len(exp) < 2:
        return exp
    last = exp[0]
    n_exp = ""
    for i in range(1, len(exp)):
        if not (last in ops and exp[i] == "+"):
            n_exp = n_exp + last
            last = exp[i]
    n_exp = n_exp + last

    return n_exp

##Funkcia na vyhodnotenie matematického výrazu zo vstupu kalkulačky
#@param exp Matematický výraz
#@return Hodnata matematického výrazu
def eval(exp: str) -> str:

    precedence = {
        "(": eval,       # 0
        "^": pow,        # 1
        "√": root,       # 2
        "s": sin,        # 3
        "c": cos,        # 4
        "t": tan,        # 5
        "!": factorial,  # 6
        "*": mul,        # 7
        "/": div,        # 8
        "%": mod,        # 9
        "+": add,        # 10
        "-": sub         # 11
    }
    precedence_keys = list(precedence.keys())
    pi = findConst("pi")
    e = findConst("e")
    exp = exp.replace("sin", "s")
    exp = exp.replace("cos", "c")
    exp = exp.replace("tan", "t")
    exp = exp.replace("mod", "%")
    exp = exp.replace("+-", "-")
    exp = exp.replace("--", "+")
    exp = exp.replace("++", "+")
    exp = exp.replace(",", ".")
    exp = exp.replace("π", numToStr(pi, 10, 10))
    exp = exp.replace("e", numToStr(e, 10, 10))
    exp = removeDoubleOperator(exp, precedence_keys)

    for i in range(len(precedence_keys)):
        while precedence_keys[i] in exp:
            ops_i = exp.index(precedence_keys[i])  # index operacie vo vyraze

            if i == 0:  # ( zatvorky
                beg_i = ops_i
                end_i = beg_i + findParenthesesMatch(exp[beg_i:])
                if end_i == beg_i:
                    raise ValueError(
                        "Matematický výraz nie je validný, nebol nájdený pár k {}".format(precedence_keys[i]))
                exp = exp[:beg_i] + eval(exp[beg_i + 1:end_i]) + exp[end_i + 1:]

            elif i == 1 or 7 <= i <= 11:  # ^,*,/,%,+,- binarne operatory

                if ops_i == 0:
                    break
                beg_i, end_i = getOperandIndexes(exp, ops_i)
                if beg_i == ops_i or end_i == ops_i:
                    raise ValueError(
                        "Matematický výraz nie je validný, chýbajú operandy k {}".format(precedence_keys[i]))
                x = strToNum(exp[beg_i: ops_i], 10)
                y = strToNum(exp[ops_i + 1: end_i + 1], 10)
                val = precedence[exp[ops_i]](x, y)
                if beg_i > 0 and exp[beg_i - 1].isdigit() and val >= 0:
                    val = "+" + numToStr(val, 10, 10)
                else:
                    val = numToStr(val, 10, 10)
                exp = exp[:beg_i] + val + exp[end_i + 1:]

            elif i == 2:  # √ root

                beg_i, end_i = getOperandIndexes(exp, ops_i)
                if exp[beg_i] == "-":
                    beg_i += 1
                if end_i == ops_i:
                    raise ValueError("Matematický výraz nie je validný, chýba operand k {}".format(precedence_keys[i]))
                x = strToNum(exp[ops_i + 1: end_i + 1], 10)
                y = strToNum(exp[beg_i: ops_i], 10) if beg_i != ops_i else 2
                val = root(x, y)
                if beg_i > 0 and exp[beg_i - 1].isdigit() and val >= 0:
                    val = "+" + numToStr(val, 10, 10)
                else:
                    val = numToStr(val, 10, 10)
                exp = exp[:beg_i] + val + exp[end_i + 1:]

            elif 3 <= i <= 5:  # s,c,t goniometricke operacie

                beg_i, end_i = getOperandIndexes(exp, ops_i)
                if end_i == ops_i:
                    raise ValueError("Matematický výraz nie je validný, chýba operand k {}".format(precedence_keys[i]))
                x = strToNum(exp[ops_i + 1: end_i + 1], 10)
                y = strToNum(exp[beg_i: ops_i], 10) if beg_i != ops_i else 1
                val = mul(precedence[exp[ops_i]](x), y)
                if beg_i > 0 and exp[beg_i - 1].isdigit() and val >= 0:
                    val = "+" + numToStr(val, 10, 10)
                else:
                    val = numToStr(val, 10, 10)
                exp = exp[:beg_i] + val + exp[end_i + 1:]

            elif i == 6:  # ! faktorial

                beg_i, end_i = getOperandIndexes(exp, ops_i)
                if exp[beg_i] == "-":
                    beg_i += 1
                if beg_i == ops_i:
                    raise ValueError("Matematický výraz nie je validný, chýba operand k {}".format(precedence_keys[i]))
                x = strToNum(exp[beg_i: ops_i], 10)
                y = strToNum(exp[ops_i + 1: end_i + 1], 10) if end_i != ops_i else 1
                exp = exp[:beg_i] + numToStr(mul(factorial(x), y), 10, 10) + exp[end_i + 1:]

            else:
                break

    return exp
