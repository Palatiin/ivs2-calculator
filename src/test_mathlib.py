import pytest
from mathlib import *

## @file test_mathlib.py
#  @brief Testy pre matematickú knižnicu
#  @author Rastislav Mozola xmozol02


def test_add():
    """
    Otestovanie funkcie add(x,y)
    """

    """Testy pre hodnotu 0"""
    assert add(0, 0) == 0
    assert add(0, 1) == 1
    assert add(-1, 0) == -1
    assert add(0, -0.5) == -0.5
    assert add(0.0, 0.0) == 0

    """Testy pre celé čísla"""
    assert add(5, 5) == 10
    assert add(-1, 1) == 0
    assert add(-1, -1) == -2

    """Testy pre desatinné čísla"""
    assert add(0.5, 0.5) == 1
    assert add(-0.5, 0.5) == 0
    assert add(-0.5, -0.5) == -1


def test_sub():
    """
    Otestovanie funkcie sub(x,y)
    """

    """Testy pre hodnotu 0"""
    assert sub(0, 0) == 0
    assert sub(0, 1) == -1
    assert sub(-1, 0) == -1
    assert sub(0, -0.5) == 0.5
    assert sub(0.0, 0.0) == 0

    """Testy pre celé čísla"""
    assert sub(5, 5) == 0
    assert sub(-1, 1) == -2
    assert sub(-1, -1) == 0

    """Testy pre desatinné čísla"""
    assert sub(0.5, 0.5) == 0
    assert sub(-0.5, 0.5) == -1
    assert sub(-0.5, -0.5) == 0


def test_mul():
    """
    Otestovanie funkcie mul(x,y)
    """

    """Testy pre hodnotu 0"""
    assert mul(0, 0) == 0
    assert mul(0, 1) == 0
    assert mul(-1, 0) == 0
    assert mul(0, -0.5) == 0
    assert mul(0.0, 0.0) == 0

    """Testy pre celé čísla"""
    assert mul(5, 5) == 25
    assert mul(-1, 1) == -1
    assert mul(-1, -1) == 1

    """Testy pre desatinné čísla"""
    assert mul(0.5, 0.5) == 0.25
    assert mul(-0.5, 0.5) == -0.25
    assert mul(-0.5, -0.5) == 0.25


def test_div():
    """
    Otestovanie funkcie div(x,y)
    """

    """Testy pre hodnotu 0"""
    with pytest.raises(Exception):
        div(0, 0)
    with pytest.raises(Exception):
        div(-1, 0)
    with pytest.raises(Exception):
        div(0.0, 0.0)

    assert div(0, 1) == 0
    assert div(0, -0.5) == 0

    """Testy pre celé čísla"""
    assert div(5, 5) == 1
    assert div(-1, 1) == -1
    assert div(-1, -1) == 1

    """Testy pre desatinné čísla"""
    assert div(0.75, 0.5) == 1.5
    assert div(0.5, 0.5) == 1
    assert div(-0.5, 0.5) == -1
    assert div(-0.5, -0.5) == 1


def test_mod():
    """
    Otestovanie funkcie mod(x,y)
    """

    """Testy pre hodnotu 0"""
    with pytest.raises(Exception):
        mod(0, 0)
    with pytest.raises(Exception):
        mod(-1, 0)
    with pytest.raises(Exception):
        mod(0.0, 0.0)

    assert mod(0, 1) == 0
    assert mod(0, -0.5) == 0

    """Testy pre celé čísla"""
    assert mod(5, 3) == 2
    assert mod(-1, 1) == 0
    assert mod(-1, -1) == 0

    """Testy pre desatinné čísla"""
    assert mod(5.5, 2) == 1.5
    assert mod(-5.5, 3) == 0.5
    with pytest.raises(Exception):
        mod(0.5, 0.25)
    with pytest.raises(Exception):
        mod(-5.5, 0.5)
    with pytest.raises(Exception):
        mod(-0.5, -0.5)

def test_pow():
    """
    Otestovanie funkcie pow(x,y)
    """

    """Testy pre hodnotu 0"""
    assert pow(0, 0) == 1
    assert pow(-1, 0) == 1
    assert pow(0.0, 0.0) == 1
    assert pow(0, 1) == 0
    with pytest.raises(Exception):
        pow(0, -0.5)

    """Testy pre celé čísla"""
    assert pow(5, 3) == 125
    assert pow(-1, 1) == -1
    assert pow(-1, -1) == -1

    """Testy pre desatinné čísla"""
    assert pow(0.64, 0.5) == 0.8

    """Odmocnina zo záporného čísla"""
    with pytest.raises(Exception):
        pow(-0.5, 0.5)
    with pytest.raises(Exception):
        pow(-0.5, -0.5)

def test_root():
    """
    Otestovanie funkcie root(x,y)
    """

    """Testy pre hodnotu 0"""
    assert root(0, 1) == 0
    with pytest.raises(Exception):
        root(0, -0.5)
    with pytest.raises(Exception):
        root(0, 0)
    with pytest.raises(Exception):
        root(0.0, 0.0)
    with pytest.raises(Exception):
        root(-1, 0)

    """Testy pre celé čísla"""
    assert root(8, 3) == 2
    assert root(-1, 1) == -1
    with pytest.raises(Exception):
        root(1, -1)

    """Testy pre desatinné čísla"""
    assert root(2.25, 2) == 1.5
    assert root(0.64, 2) == 0.8

    """Odmocnina zo záporného čísla"""
    with pytest.raises(Exception):
        root(-0.5, 2)
    with pytest.raises(Exception):
        root(-0.5, -0.5)
