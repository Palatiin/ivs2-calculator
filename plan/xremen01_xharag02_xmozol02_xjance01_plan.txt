SZÉKESFEHERVÁRBALATONSZAVADISÓSTÓ A.S. 
xmozol02, xjance01, xharag02, xremen01

IVS - Projekt 2 - Tímová spolupráca - Kalkulačka
OS: Ubuntu 20.04

Datum: 18.3.2021

================================================================================

TERMÍNY:

17.3.    repozitár na GitLab-e
18.3.    plán
1.4.	 návrh konštrukcie
5.4.     testy, základné operácie
10.4.	 pokročilé operácie, Makefile
16.4.    GUI, ovládanie, profilling
22.4.    dokumentácia, nápoveda, manuál, inštalátor/odinštalátor
29.4.    finálny produkt, mockup, ladenie detailov
21.5.    problémy
21.5.    inštalácie

================================================================================

ROZDELENIE ÚLOH:

xjance01:
	Základné matematické operácie
	Makefile
	Nápoveda
	Manuál

xmozol02:
	Správa repozitára
	Testovanie
	GUI
	Prezentácia	

xharag02:
	Návrh komunikácie programových súčastí
	Pokročilé matematické operácie
	Profiling
	Inštalátor/odinštalátor

xremen01:
	Pokročilé matematické operácie
	Ovládanie kalkulačky
	Kontrola práce
	Dokumentácia

Mockup 
- budú sa na ňom podielať všetci členovia tímu

Dokumentácia
- každý člen tímu bude viesť dokumentáciu a nakoniec sa najlepšie časti
zjednotia do výslednej dokumentácie

================================================================================

KOMUNIKAČNÝ KANÁL: 

Discord
Slúži ako komunikačný kanál vo forme chatu/fóra. Umožňuje aj skupinové hovory 
formou ktorých bude tím komunikovať riešenie projektu.

================================================================================

Vedúci tímu: xremen01 (Matúš Remeň)
Zastupuje tím pri komunikácii s vyučujúcimi a dozerá na hladký priebeh vývoja 
projektu, spoluprácu a rovnomerne rozdeluje úlohy medzi členov tímu.

================================================================================

SYSTÉM PRE SPRÁVU VERZIÍ A HOSTING: GitLab

Názov repozitáru: IVS2-Calculator
https://gitlab.com/sz-kesfeherv-rbalatonszavadis-st-as/ivs2-calculator

Repozitár je zdielaný s užívatelom ivskontrola (ivs.kontrola@gmail.com)

